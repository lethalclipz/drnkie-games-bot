package messenger

import "gitlab.com/lethalclipz/drnkie-games-bot/types"

// DiscordMessenger represents the state machine's connection to the Discord bot.
type DiscordMessenger interface {
	GiveUserGameChannelPermissions(userID string) error
	TakeAwayUserGameChannelPermissions(userID string) error
	AnnounceGameInLobby(caller, gameType string) error
	SetStatus(status string) error
	MentionNewPlayers(users ...*types.ConfirmingUser) error
	AnnouceKickInGameChannel(userID string) error
	ReactPositivelyToMessage(channelID, messageID string) error
	SendMessage(channelID, message string) error
}
