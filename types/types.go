package types

import (
	"context"
	"sync"

	"github.com/qor/transition"
)

type State struct {
	GameType          int
	QueuedPlayers     []*User
	PlayingPlayers    []*User
	BlockedPlayers    []*User
	ForcedPlayers     []*User
	ConfirmingPlayers []*ConfirmingUser
	CreatedBy         string
	transition.Transition
	sync.Mutex
}

func (s *State) GameTypePretty(uppercase bool) string {
	if uppercase {
		if s.GameType == 0 {
			return "Subscriber-only"
		} else if s.GameType == 1 {
			return "Open"
		} else {
			return ""
		}
	} else {
		if s.GameType == 0 {
			return "subscriber-only"
		} else if s.GameType == 1 {
			return "open"
		} else {
			return ""
		}
	}
}

type User struct {
	ID string
}

type ConfirmingUser struct {
	*User
	Confirmed bool
	Cancel    context.CancelFunc
	Ctx       context.Context
}
