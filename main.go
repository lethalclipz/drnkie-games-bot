package main

import (
	"os"
	"os/signal"
	"syscall"

	log "github.com/sirupsen/logrus"

	"gitlab.com/lethalclipz/drnkie-games-bot/config"
	"gitlab.com/lethalclipz/drnkie-games-bot/discord"
	"gitlab.com/lethalclipz/drnkie-games-bot/messagehandler"
	"gitlab.com/lethalclipz/drnkie-games-bot/state"
)

func main() {
	log.WithFields(log.Fields{
		"guild_id":               config.Config.GuildID,
		"max_party_size":         config.Config.MaxPartySize,
		"confirm_timeout_length": config.Config.ConfirmTimeoutLength,
		"enter_queue_command":    config.Config.EnterQueueCommand,
	}).Info("Bot started")

	err := discord.Initialize(config.Config.Token)
	if err != nil {
		log.WithError(err).Fatal("Failed to connect to Discord session")
	}

	discord.Session.AddHandler(messagehandler.HandleMessage)
	state.DiscordConnection = discord.Session
	state.MaxPartySize = config.Config.MaxPartySize

	log.WithField("guild_id", config.Config.GuildID).Debug("Requesting guild member chunk...")
	err = discord.Session.RequestGuildMembers(config.Config.GuildID, "", 0)
	if err != nil {
		log.WithError(err).Error("Failed to get member chunk")
	} else {
		log.WithField("guild_id", config.Config.GuildID).Info("Successfully fetched guild member chunk")
	}

	// Wait here until CTRL-C or other term signal is received.
	log.Info("Bot is now running.")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	// Cleanly close down the Discord session.
	discord.Stop()
	log.Info("Bot stopped")
}
