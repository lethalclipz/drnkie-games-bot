package discord

import (
	"fmt"

	"gitlab.com/lethalclipz/drnkie-games-bot/state"

	"github.com/bwmarrin/discordgo"
	log "github.com/sirupsen/logrus"

	"gitlab.com/lethalclipz/drnkie-games-bot/config"
	"gitlab.com/lethalclipz/drnkie-games-bot/types"
)

// DiscordSession is a session type that implements DiscordConnection
type DiscordSession struct {
	*discordgo.Session
}

// Session is the main reference to the DiscordSession for this connection
var Session *DiscordSession

// Initialize initializes the bot, connecting to the websocket and making the session available for use.
func Initialize(token string) error {
	session, err := discordgo.New(token)
	if err != nil {
		return err
	}

	Session = &DiscordSession{session}

	err = Session.Open()
	if err == nil {
		log.WithField("state", state.GameState.GetState()).Info("Successfully connected to Discord.")
	}
	return err
}

// Stop stops the bot's connection to the Discord gateway
func Stop() {
	Session.Close()
	log.WithField("state", state.GameState.GetState()).Info("Stopped Discord session.")
}

// GiveUserGameChannelPermissions gives the user with userID read and send permissions on the game channel
func (s *DiscordSession) GiveUserGameChannelPermissions(userID string) error {
	return s.ChannelPermissionSet(config.Config.Channels.GameChannel, userID, "member", discordgo.PermissionReadMessages|discordgo.PermissionSendMessages|discordgo.PermissionReadMessageHistory, 0)
}

// TakeAwayUserGameChannelPermissions takes away read and send permissions on the game channel for the user with userID
func (s *DiscordSession) TakeAwayUserGameChannelPermissions(userID string) error {
	return s.ChannelPermissionSet(config.Config.Channels.GameChannel, userID, "member", 0, discordgo.PermissionReadMessages|discordgo.PermissionReadMessageHistory|discordgo.PermissionSendMessages)
}

// AnnounceGameInLobby announces a new game in the lobby channel
func (s *DiscordSession) AnnounceGameInLobby(caller, gameType string) error {
	var tagRole string
	if state.GameState.GameType == state.TypeSubGame {
		tagRole = fmt.Sprintf("<@&%s>", config.Config.Roles.SubscriberRole)
		data := struct {
			Mentionable bool `json:"mentionable"`
		}{true}
		_, err := s.RequestWithBucketID("PATCH", discordgo.EndpointGuildRole(config.Config.GuildID, config.Config.Roles.SubscriberRole), data, discordgo.EndpointGuildRole(config.Config.GuildID, ""))
		if err != nil {
			log.WithField("state", state.GameState.GetState()).WithError(err).Error("Failed to change role mentionability")
		}
		dataFalse := struct {
			Mentionable bool `json:"mentionable"`
		}{false}
		defer s.RequestWithBucketID("PATCH", discordgo.EndpointGuildRole(config.Config.GuildID, config.Config.Roles.SubscriberRole), dataFalse, discordgo.EndpointGuildRole(config.Config.GuildID, ""))
	} else {
		tagRole = "@here"
	}
	message := fmt.Sprintf("Welcome %s! **%s** game created by **%s**. Type **!%s** to enter the queue!", tagRole, gameType, caller, config.Config.EnterQueueCommand)
	_, err := s.ChannelMessageSend(config.Config.Channels.LobbyChannel, message)
	return err
}

// SetStatus sets the bot's status to status
func (s *DiscordSession) SetStatus(status string) error {
	return s.UpdateStatus(0, status)
}

// MentionNewPlayers sends a message mentioning the new players in a game in the game channel
func (s *DiscordSession) MentionNewPlayers(users ...*types.ConfirmingUser) error {
	usersString := ""
	for i, user := range users {
		dUser := discordgo.User{ID: user.ID}
		usersString += dUser.Mention()

		// Only add if user not last user in list
		if i < len(users)-1 {
			usersString += ", "
		}
	}

	message := fmt.Sprintf("Welcome to the game %s! You have **%d** seconds to **!confirm** your spot, or else it will be forfeited.", usersString, config.Config.ConfirmTimeoutLength)
	_, err := s.ChannelMessageSend(config.Config.Channels.GameChannel, message)
	return err
}

// AnnouceKickInGameChannel announces when a player has been kicked in the game channel
func (s *DiscordSession) AnnouceKickInGameChannel(userID string) error {
	user := discordgo.User{ID: userID}
	message := fmt.Sprintf("%s has been kicked from the game due to inactivity. Searching for new player...", user.Mention())
	_, err := s.ChannelMessageSend(config.Config.Channels.GameChannel, message)
	return err
}

// ReactPositivelyToMessage reacts to a message with :ok_hand: emoji
func (s *DiscordSession) ReactPositivelyToMessage(channelID, messageID string) error {
	return s.MessageReactionAdd(channelID, messageID, "👌")
}

// SendMessage sends a message with content message to channel channelID
func (s *DiscordSession) SendMessage(channelID, message string) error {
	_, err := s.ChannelMessageSend(channelID, message)
	return err
}
