package state

import (
	"context"
	"errors"
	"fmt"
	"math/rand"
	"time"

	"github.com/jinzhu/gorm"
	"github.com/qor/transition"
	log "github.com/sirupsen/logrus"

	"gitlab.com/lethalclipz/drnkie-games-bot/config"
	"gitlab.com/lethalclipz/drnkie-games-bot/messenger"
	"gitlab.com/lethalclipz/drnkie-games-bot/types"
)

// GameState is the reference to the current State of the game
var GameState *types.State

// StateMachine is the current state macchine controlling the game
var StateMachine *transition.StateMachine

// MaxPartySize is the maximum party size of the game (not including host)
var MaxPartySize int

// Possible game types
const (
	TypeSubGame = iota
	TypeOpenGame
	TypeNULL
)

// Possible game states
const (
	StateDormant           = "Dormant"
	StateWaitingForPlayers = "Waiting For Players"
	StateConfirming        = "Confirming"
	StateInGame            = "In-game"
)

// DiscordConnection is the state's connection to Discord
var DiscordConnection messenger.DiscordMessenger

func init() {
	GameState = &types.State{}
	StateMachine = transition.New(GameState)
	StateMachine.Initial(StateDormant)

	setupEvent := StateMachine.Event("setup")
	setupEvent.To(StateWaitingForPlayers).From(StateDormant).After(func(state interface{}, tx *gorm.DB) error {
		log.WithField("state", state.(*types.State).GetState()).Info("Transitioning to Waiting for Players state")
		DiscordConnection.SetStatus("waiting for players")
		DiscordConnection.AnnounceGameInLobby(state.(*types.State).CreatedBy, state.(*types.State).GameTypePretty(true))
		message := fmt.Sprintf("Success! Created **%s** game, and announced in lobby.", state.(*types.State).GameTypePretty(false))
		return DiscordConnection.SendMessage(config.Config.Channels.AdminChannel, message)
	})
	setupEvent.To(StateWaitingForPlayers).From(StateConfirming).After(func(state interface{}, tx *gorm.DB) error {
		log.WithField("state", state.(*types.State).GetState()).Info("Transitioning to Waiting for Players state")
		DiscordConnection.SetStatus("waiting for players")
		return DiscordConnection.SendMessage(config.Config.Channels.AdminChannel, "No players left in queue, going back to waiting for players state.")
	})

	StateMachine.State(StateConfirming).Enter(func(state interface{}, tx *gorm.DB) error {
		log.WithField("state", state.(*types.State).GetState()).Info("Transitioning to Confirming state")
		gameState := state.(*types.State)
		newPlayers := getRandomPlayers(MaxPartySize-len(gameState.PlayingPlayers)-len(gameState.ConfirmingPlayers), gameState)

		if len(newPlayers) <= 0 {
			DiscordConnection.SendMessage(config.Config.Channels.GameChannel, "No available players remaining, starting game anyways.")
			StateMachine.Trigger("all-confirmed", gameState, nil)
			return nil
		}

		for _, v := range newPlayers {
			addPlayerToConfirming(v, gameState)
		}

		DiscordConnection.SetStatus("confirming players")
		DiscordConnection.MentionNewPlayers(gameState.ConfirmingPlayers...)

		go func(s *types.State) {
			userChannel := make(chan *types.ConfirmingUser)
			for _, v := range s.ConfirmingPlayers {
				go waitForPlayerToConfirm(userChannel, v)
			}

			for {
				select {
				case user := <-userChannel:
					if s.GetState() == StateConfirming {
						if user.Confirmed {
							log.WithFields(log.Fields{"user": user.ID, "state": gameState.GetState()}).Info("Player Confirmed")
							if allPlayersConfirmed(s) {
								log.Info("All players confirmed!")
								StateMachine.Trigger("all-confirmed", s, nil)
								return
							}
							log.WithField("state", gameState.GetState()).Info("Still waiting on some players to confirm.")
						} else {
							log.WithFields(log.Fields{"user": user.ID, "state": gameState.GetState()}).Info("Player timed out")
							removePlayerFromConfirming(user, s)
							DiscordConnection.AnnouceKickInGameChannel(user.ID)
							if len(s.QueuedPlayers)+len(s.ForcedPlayers) > 0 {
								s.Lock()
								player := getRandomPlayers(1, s)[0]
								confirmingPlayer := addPlayerToConfirming(player, s)
								s.Unlock()
								DiscordConnection.MentionNewPlayers(confirmingPlayer)
								go waitForPlayerToConfirm(userChannel, confirmingPlayer)
							} else {
								if len(s.ConfirmingPlayers) > 0 {
									if allPlayersConfirmed(s) {
										DiscordConnection.SendMessage(config.Config.Channels.GameChannel, "No more queued players. Game starting without full lobby.")
										StateMachine.Trigger("all-confirmed", s, nil)
										return
									}
								} else {
									DiscordConnection.SendMessage(config.Config.Channels.GameChannel, "No players left to play. Canceling game.")
									StateMachine.Trigger("stop", s, nil)
									return
								}
							}
						}
					}
				}
			}
		}(gameState)
		return nil
	})

	StateMachine.Event("start").To(StateConfirming).From(StateWaitingForPlayers, StateInGame).Before(func(state interface{}, tx *gorm.DB) error {
		if len(state.(*types.State).QueuedPlayers)+len(state.(*types.State).ForcedPlayers) == 0 {
			return errors.New("no players in queue with which to start match")
		}
		for _, user := range state.(*types.State).PlayingPlayers {
			state.(*types.State).BlockedPlayers = append(state.(*types.State).BlockedPlayers, user)
			go func(user *types.User) {
				err := DiscordConnection.TakeAwayUserGameChannelPermissions(user.ID)
				if err != nil {
					log.WithField("state", state.(*types.State).GetState()).WithError(err).Error("Failed to remove permissions from user")
				}
			}(user)
		}
		state.(*types.State).PlayingPlayers = []*types.User{}
		return nil
	})

	StateMachine.Event("player-kicked").To(StateConfirming).From(StateInGame).Before(func(state interface{}, tx *gorm.DB) error {
		DiscordConnection.SendMessage(config.Config.Channels.GameChannel, "Player kicked.")
		return nil
	})

	StateMachine.Event("all-confirmed").To(StateInGame).From(StateConfirming).Before(func(state interface{}, tx *gorm.DB) error {
		gameState := state.(*types.State)
		for _, v := range gameState.ConfirmingPlayers {
			gameState.PlayingPlayers = append(gameState.PlayingPlayers, v.User)
		}
		gameState.ConfirmingPlayers = []*types.ConfirmingUser{}
		DiscordConnection.SendMessage(config.Config.Channels.GameChannel, "All players successfully confirmed. Game is now underway.")
		DiscordConnection.SetStatus("in-game")
		return nil
	})

	StateMachine.Event("stop").To(StateDormant).Before(func(state interface{}, tx *gorm.DB) error {
		if GameState.GetState() != StateDormant {
			DiscordConnection.SendMessage(config.Config.Channels.AdminChannel, "Game stopped.")
			gameState := state.(*types.State)
			for _, v := range gameState.PlayingPlayers {
				DiscordConnection.TakeAwayUserGameChannelPermissions(v.ID)
			}
			for _, v := range gameState.ConfirmingPlayers {
				DiscordConnection.TakeAwayUserGameChannelPermissions(v.ID)
			}
			gameState.PlayingPlayers = []*types.User{}
			gameState.QueuedPlayers = []*types.User{}
			gameState.BlockedPlayers = []*types.User{}
			gameState.ConfirmingPlayers = []*types.ConfirmingUser{}
			gameState.CreatedBy = ""
			gameState.GameType = TypeNULL
			err := DiscordConnection.SetStatus("")
			if err != nil {
				log.WithField("state", gameState.GetState()).WithError(err).Error("Failed to set status")
			}
			return nil
		}

		return errors.New("no game currently running")
	})

	GameState.SetState(StateDormant)
}

func waitForPlayerToConfirm(user chan *types.ConfirmingUser, cp *types.ConfirmingUser) {
	for {
		select {
		case <-cp.Ctx.Done():
			user <- cp
			return
		}
	}
}

func getRandomPlayers(amount int, state *types.State) []*types.User {
	var players []*types.User

	for i := 0; i < amount; i++ {
		if len(state.ForcedPlayers) <= i {
			break
		}

		queuedIndex := GetIndexOfPlayerInSlice(state.ForcedPlayers[i], state.QueuedPlayers)
		state.QueuedPlayers = append(state.QueuedPlayers[:queuedIndex], state.QueuedPlayers[queuedIndex+1:]...)
		players = append(players, state.ForcedPlayers[i])
		state.ForcedPlayers = append(state.ForcedPlayers[:i], state.ForcedPlayers[i+1:]...)
	}

	amountRemaining := amount - len(players)

	for i := 0; i < amountRemaining; i++ {
		blocked := true
		for blocked {
			if len(state.QueuedPlayers) <= 0 {
				return players
			}
			index := rand.Intn(len(state.QueuedPlayers))
			player := state.QueuedPlayers[index]
			if !playerBlocked(player, state) {
				players = append(players, player)
				state.QueuedPlayers = append(state.QueuedPlayers[:index], state.QueuedPlayers[index+1:]...)
				blocked = false
			}
		}
	}
	return players
}

func addPlayerToConfirming(user *types.User, state *types.State) *types.ConfirmingUser {
	timeoutLength := time.Duration(config.Config.ConfirmTimeoutLength) * time.Second
	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, timeoutLength)
	confirmingPlayer := types.ConfirmingUser{User: user, Cancel: cancel, Ctx: ctx}
	state.ConfirmingPlayers = append(state.ConfirmingPlayers, &confirmingPlayer)

	err := DiscordConnection.GiveUserGameChannelPermissions(user.ID)
	if err != nil {
		log.WithField("state", state.GetState()).WithError(err).Error("Failed to give user permissions")
	}

	return &confirmingPlayer
}

func removePlayerFromConfirming(user *types.ConfirmingUser, state *types.State) {
	for i, v := range state.ConfirmingPlayers {
		if user.ID == v.ID {
			state.ConfirmingPlayers = append(state.ConfirmingPlayers[:i], state.ConfirmingPlayers[i+1:]...)
			DiscordConnection.TakeAwayUserGameChannelPermissions(user.ID)
		}
	}
}

// ConfirmPlayer confirms the user with ID userID
func ConfirmPlayer(userID string, state *types.State) error {
	for _, v := range state.ConfirmingPlayers {
		if userID == v.ID {
			v.Confirmed = true
			v.Cancel()
			return nil
		}
	}
	return errors.New("failed to find user to confirm")
}

func allPlayersConfirmed(state *types.State) bool {
	if len(state.ConfirmingPlayers) <= 0 {
		return false
	}

	for _, v := range state.ConfirmingPlayers {
		if !v.Confirmed {
			return false
		}
	}
	return true
}

func playerBlocked(user *types.User, state *types.State) bool {
	for _, v := range state.BlockedPlayers {
		if v.ID == user.ID {
			return true
		}
	}
	return false
}

func GetIndexOfPlayerInSlice(user *types.User, slice []*types.User) int {
	for i, v := range slice {
		if user.ID == v.ID {
			return i
		}
	}
	return -1
}
