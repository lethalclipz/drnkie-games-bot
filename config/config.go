package config

import (
	"github.com/BurntSushi/toml"
	"github.com/kz/discordrus"
	colorable "github.com/mattn/go-colorable"
	log "github.com/sirupsen/logrus"
)

// Config represents the configuration state of the bot.
var Config Configuration

// Configuration represents a configuration file and its details.
type Configuration struct {
	Token                string   `toml:"bot_token"`
	GuildID              string   `toml:"guild_id"`
	WebhookURL           string   `toml:"webhook_url"`
	MaxPartySize         int      `toml:"max_party_size"`
	ConfirmTimeoutLength int      `toml:"confirm_timeout_length"`
	EnterQueueCommand    string   `toml:"enter_queue_command"`
	Channels             Channels `toml:"channels"`
	Roles                Roles    `toml:"roles"`
}

// Channels represents the channels used by the bot.
type Channels struct {
	AdminChannel string `toml:"admin_channel_id"`
	GameChannel  string `toml:"game_channel_id"`
	LobbyChannel string `toml:"lobby_channel_id"`
}

// Roles represents the roles used by the bot.
type Roles struct {
	AdminRole      string `toml:"admin_role_id"`
	SubscriberRole string `toml:"sub_role_id"`
}

func init() {
	log.SetFormatter(&log.TextFormatter{ForceColors: true})
	log.SetOutput(colorable.NewColorableStdout())

	if _, err := toml.DecodeFile("settings.toml", &Config); err != nil {
		log.WithError(err).Fatal("Failed to load configuration information")
	}

	log.AddHook(discordrus.NewHook(
		Config.WebhookURL,
		log.InfoLevel,
		&discordrus.Opts{},
	))
}
