package messagehandler

import (
	"fmt"
	"strings"

	"github.com/bwmarrin/discordgo"
	log "github.com/sirupsen/logrus"

	"gitlab.com/lethalclipz/drnkie-games-bot/config"
	"gitlab.com/lethalclipz/drnkie-games-bot/discord"
	"gitlab.com/lethalclipz/drnkie-games-bot/state"
	"gitlab.com/lethalclipz/drnkie-games-bot/types"
)

var commands map[string]func(s *discordgo.Session, m *discordgo.MessageCreate)

func init() {
	addFunctionsToMap()
}

func addFunctionsToMap() {
	commands = make(map[string]func(s *discordgo.Session, m *discordgo.MessageCreate))
	commands["setup"] = setupGame
	commands["play"] = addToQueue
	commands["status"] = sendStatus
	commands["start"] = startGame
	commands["confirm"] = confirmPlayer
	commands["stop"] = stopGame
	commands["force"] = forcePlayer
	commands["kick"] = kickPlayer
}

// HandleMessage is the main entry point for message handling
func HandleMessage(s *discordgo.Session, m *discordgo.MessageCreate) {
	if strings.HasPrefix(m.Content, "!") {
		function, ok := commands[strings.ToLower(strings.TrimPrefix(strings.Fields(m.Content)[0], "!"))]
		if ok {
			function(s, m)
		}
	}
}

func setupGame(s *discordgo.Session, m *discordgo.MessageCreate) {
	if isInAdminContext(m.ChannelID, m.Author) {
		log.WithField("state", state.GameState.GetState()).Info("Game setup called")
		stringFields := strings.Fields(m.Content)
		if len(stringFields) > 1 {
			if stringFields[1] == "sub" || stringFields[1] == "open" {
				log.WithFields(log.Fields{"type": stringFields[1], "state": state.GameState.GetState()}).Info("Got command to set up game")
				var gameType int
				if stringFields[1] == "sub" {
					gameType = state.TypeSubGame
				} else if stringFields[1] == "open" {
					gameType = state.TypeOpenGame
				}
				state.GameState.GameType = gameType
				state.GameState.CreatedBy = m.Author.Username
				err := state.StateMachine.Trigger("setup", state.GameState, nil)
				if err != nil {
					_, err = s.ChannelMessageSend(config.Config.Channels.AdminChannel, fmt.Sprintf("Error: failed to set up game: %v", err))
					if err != nil {
						log.WithField("state", state.GameState.GetState()).WithError(err).Error("Message send failed")
					}
					return
				}
			} else {
				log.WithFields(log.Fields{"field": stringFields[1], "state": state.GameState.GetState()}).Warn("Parameter to !setup incorrect")
				_, err := s.ChannelMessageSend(config.Config.Channels.AdminChannel, "Error: send either **sub** or **open** as a parameter to !setup.")
				if err != nil {
					log.WithField("state", state.GameState.GetState()).WithError(err).Error("Message send failed")
				}
				return
			}
		} else {
			log.Info("Got no parameters to !setup call")
			_, err := s.ChannelMessageSend(config.Config.Channels.AdminChannel, "Error: send either **sub** or **open** as a parameter to !setup.")
			if err != nil {
				log.WithField("state", state.GameState.GetState()).WithError(err).Error("Message send failed")
			}
			return
		}
	}
}

func addToQueue(s *discordgo.Session, m *discordgo.MessageCreate) {
	if m.ChannelID == config.Config.Channels.LobbyChannel {
		if state.GameState.GetState() != state.StateDormant {
			subRequired := state.GameState.GameType == state.TypeSubGame

			if subRequired && !userHasRole(m.Author, config.Config.Roles.SubscriberRole) {
				state.DiscordConnection.SendMessage(m.ChannelID, fmt.Sprintf("%s, this is a **subscriber-only** game.", m.Author.Mention()))
				return
			}

			if !userInQueue(&types.User{ID: m.Author.ID}) {
				state.GameState.QueuedPlayers = append(state.GameState.QueuedPlayers, &types.User{ID: m.Author.ID})
				err := state.DiscordConnection.ReactPositivelyToMessage(m.ChannelID, m.ID)
				if err != nil {
					log.WithField("state", state.GameState.GetState()).WithError(err).Error("Failed to react to message")
					return
				}
				log.WithFields(log.Fields{"user": m.Author.Username, "state": state.GameState.GetState()}).Info("Added user to queue")
			} else {
				state.DiscordConnection.SendMessage(m.ChannelID, fmt.Sprintf("%s, you're already in the queue!", m.Author.Mention()))
				return
			}
		} else {
			state.DiscordConnection.SendMessage(m.ChannelID, fmt.Sprintf("%s, no game is currently running.", m.Author.Mention()))
			return
		}
	}
}

func sendStatus(s *discordgo.Session, m *discordgo.MessageCreate) {
	var status discordgo.MessageEmbed
	gameState := state.GameState.GetState()
	fields := []*discordgo.MessageEmbedField{}
	fields = append(fields, &discordgo.MessageEmbedField{
		Name:  "State",
		Value: gameState,
	})
	if gameState == state.StateDormant {
		status = discordgo.MessageEmbed{
			Title:  "Game Status",
			Color:  0xad0a0a, // red
			Fields: fields,
		}
	} else if gameState == state.StateWaitingForPlayers {
		queuedPlayers := state.GameState.QueuedPlayers
		forcedPlayers := state.GameState.ForcedPlayers

		if len(queuedPlayers) > 0 {
			queuedPlayersString := prettyUserStringNewLine(queuedPlayers)
			fields = append(fields, &discordgo.MessageEmbedField{
				Name:  "Players in Queue",
				Value: queuedPlayersString,
			})
		}
		if len(forcedPlayers) > 0 {
			forcedPlayersString := prettyUserStringNewLine(forcedPlayers)
			fields = append(fields, &discordgo.MessageEmbedField{
				Name:  "Forced Players",
				Value: forcedPlayersString,
			})
		}

		status = discordgo.MessageEmbed{
			Title:  "Game Status",
			Color:  0x16d9f7, // blue
			Fields: fields,
		}
	} else if gameState == state.StateConfirming {
		confirmingPlayers := state.GameState.ConfirmingPlayers
		confirmingUsers := []*types.User{}
		for _, v := range confirmingPlayers {
			if !v.Confirmed {
				confirmingUsers = append(confirmingUsers, v.User)
			}
		}

		if len(confirmingUsers) > 0 {
			confirmingUsersString := prettyUserStringNewLine(confirmingUsers)
			fields = append(fields, &discordgo.MessageEmbedField{
				Name:  "Unconfirmed Players",
				Value: confirmingUsersString,
			})
		}

		status = discordgo.MessageEmbed{
			Title:  "Game Status",
			Color:  0xf700e2, // pink
			Fields: fields,
		}
	} else if gameState == state.StateInGame {
		playingPlayers := state.GameState.PlayingPlayers
		queuedPlayers := state.GameState.QueuedPlayers

		if len(playingPlayers) > 0 {
			playingPlayersString := prettyUserStringNewLine(playingPlayers)
			fields = append(fields, &discordgo.MessageEmbedField{
				Name:  "Current Players",
				Value: playingPlayersString,
			})
		}

		if len(queuedPlayers) > 0 {
			queuedPlayersString := prettyUserStringNewLine(queuedPlayers)
			fields = append(fields, &discordgo.MessageEmbedField{
				Name:  "Players in Queue",
				Value: queuedPlayersString,
			})
		}

		status = discordgo.MessageEmbed{
			Title:  "Game Status",
			Color:  0x15ef09, // green
			Fields: fields,
		}
	} else {
		log.WithField("state", gameState).Warn("Invalid state for embed")
	}

	_, err := s.ChannelMessageSendEmbed(m.ChannelID, &status)
	if err != nil {
		log.WithField("state", gameState).WithError(err).Error("Failed to send status")
	}
}

func startGame(s *discordgo.Session, m *discordgo.MessageCreate) {
	if isInAdminContext(m.ChannelID, m.Author) {
		err := state.StateMachine.Trigger("start", state.GameState, nil)
		if err != nil {
			s.ChannelMessageSend(m.ChannelID, fmt.Sprintf("Error: failed to start game: %v", err))
			log.WithField("state", state.GameState.GetState()).WithError(err).Warn("Failed to transition to In-game state")
			return
		}

		usersString := ""
		for i, v := range state.GameState.ConfirmingPlayers {
			user := discordgo.User{ID: v.ID}
			usersString += user.Mention()

			// Only add if user not last user in list
			if i < len(state.GameState.ConfirmingPlayers)-1 {
				usersString += ", "
			}
		}
		s.ChannelMessageSend(m.ChannelID, fmt.Sprintf("Success! Started game with player(s): %s.", usersString))
	}
}

func confirmPlayer(s *discordgo.Session, m *discordgo.MessageCreate) {
	if m.ChannelID == config.Config.Channels.GameChannel {
		err := state.ConfirmPlayer(m.Author.ID, state.GameState)
		if err != nil {
			s.ChannelMessageSend(m.ChannelID, fmt.Sprintf("%s, you're not in this game.", m.Author.Mention()))
			return
		}
		state.DiscordConnection.ReactPositivelyToMessage(m.ChannelID, m.ID)
	}
}

func stopGame(s *discordgo.Session, m *discordgo.MessageCreate) {
	if isInAdminContext(m.ChannelID, m.Author) {
		err := state.StateMachine.Trigger("stop", state.GameState, nil)
		if err != nil {
			state.DiscordConnection.SendMessage(m.ChannelID, fmt.Sprintf("Error: failed to stop game: %v", err))
			return
		}
	}
}

func forcePlayer(s *discordgo.Session, m *discordgo.MessageCreate) {
	if isInAdminContext(m.ChannelID, m.Author) {
		players := m.Mentions
		if len(players) <= 0 {
			state.DiscordConnection.SendMessage(m.ChannelID, "Error: you must tag one or more players")
			return
		}
		var unsuccessfulPlayers []*discordgo.User
		var successfulPlayers []*discordgo.User
		for _, v := range players {
			if userInQueue(&types.User{ID: v.ID}) {
				successfulPlayers = append(successfulPlayers, v)
			} else {
				unsuccessfulPlayers = append(unsuccessfulPlayers, v)
			}
		}
		if len(successfulPlayers) > 0 {
			state.DiscordConnection.SendMessage(m.ChannelID, fmt.Sprintf("Player(s) successfully forced into next game: %s", prettyUserString(successfulPlayers)))
			for _, v := range successfulPlayers {
				state.GameState.ForcedPlayers = append(state.GameState.ForcedPlayers, &types.User{ID: v.ID})
			}
		}
		if len(unsuccessfulPlayers) > 0 {
			state.DiscordConnection.SendMessage(m.ChannelID, fmt.Sprintf("Error: player(s) must be in queue to be forced into game: %s", prettyUserString(unsuccessfulPlayers)))
		}
	}
}

func kickPlayer(s *discordgo.Session, m *discordgo.MessageCreate) {
	if userHasRole(m.Author, config.Config.Roles.AdminRole) && (m.ChannelID == config.Config.Channels.AdminChannel || m.ChannelID == config.Config.Channels.GameChannel) {
		if state.GameState.GetState() == state.StateInGame {
			players := m.Mentions
			if len(players) != 1 {
				state.DiscordConnection.SendMessage(m.ChannelID, "Error: you must tag one player")
				return
			}

			player := players[0]
			user := &types.User{ID: player.ID}

			if userInGame(user) {
				index := state.GetIndexOfPlayerInSlice(user, state.GameState.PlayingPlayers)
				state.GameState.PlayingPlayers = append(state.GameState.PlayingPlayers[:index], state.GameState.PlayingPlayers[index+1:]...)
				err := state.DiscordConnection.TakeAwayUserGameChannelPermissions(user.ID)
				if err != nil {
					log.WithField("state", state.GameState.GetState()).WithError(err).Error("Failed to take away user permissions")
				}

				err = state.StateMachine.Trigger("player-kicked", state.GameState, nil)
				if err != nil {
					state.DiscordConnection.SendMessage(m.ChannelID, fmt.Sprintf("Error: %v", err))
					return
				}
			} else {
				state.DiscordConnection.SendMessage(m.ChannelID, "Error: user must be in this game to be kicked")
				return
			}
		} else {
			state.DiscordConnection.SendMessage(m.ChannelID, "Error: game must be running to kick player")
			return
		}
	}
}

func userHasRole(user *discordgo.User, roleID string) bool {
	var member *discordgo.Member
	member, err := discord.Session.State.Member(config.Config.GuildID, user.ID)
	if err != nil {
		member, err = discord.Session.GuildMember(config.Config.GuildID, user.ID)
		if err != nil {
			log.WithField("state", state.GameState.GetState()).Error("Failed to get user in setup function")
			return false
		}
	}

	for _, role := range member.Roles {
		if role == roleID {
			return true
		}
	}

	return false
}

func userInQueue(user *types.User) bool {
	for _, v := range state.GameState.QueuedPlayers {
		if v.ID == user.ID {
			return true
		}
	}
	return false
}

func userInGame(user *types.User) bool {
	for _, v := range state.GameState.PlayingPlayers {
		if v.ID == user.ID {
			return true
		}
	}
	return false
}

func prettyUserString(users []*discordgo.User) string {
	usersString := ""
	for i, user := range users {
		usersString += user.Mention()

		// Only add if user not last user in list
		if i < len(users)-1 {
			usersString += ", "
		}
	}

	return usersString
}

func prettyUserStringNewLine(users []*types.User) string {
	usersString := ""
	for i, user := range users {
		usersString += fmt.Sprintf("• <@%s>", user.ID)

		// Only add if user not last user in list
		if i < len(users)-1 {
			usersString += "\n"
		}
	}

	return usersString
}

func isInAdminContext(channelID string, user *discordgo.User) bool {
	return channelID == config.Config.Channels.AdminChannel && userHasRole(user, config.Config.Roles.AdminRole)
}
